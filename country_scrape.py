# Imports

from bs4 import BeautifulSoup
import requests
import csv


#Pulling http data from url
source = requests.get('https://scrapethissite.com/pages/simple').text

#sending that data through bs4 and parsing html with lxml
soup = BeautifulSoup(source, 'lxml')

# print(soup.prettify())

#Finding class country on page
country = soup.find('div', class_ ='country')
# print(country.prettify())

#Opening csv file and writing headers to csv
csv_file = open('countrydata.csv', 'w', encoding="utf-8", newline='')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['Country Name', 'Capital City', 'Population', 'Area (km sq)'])

# Try blocks to collect data and catch empty exceptions thrown if any exist
for country in soup.find_all('div', class_ ='country'):
    try:
        country_name = country.h3.text.strip()
    except Exception as countryMissingE:
        country_name = None

    print(country_name)


    try:
        capital = country.find('span', 'country-capital').text
    except Exception as capitalMissingE:
        capital = None

    print(capital)

    try:
        population = country.find('span', 'country-population').text
    except Exception as populationMissingE:
        population = None

    print(population)

    try:
        area = country.find('span', 'country-area').text
    except Exception as areaMissingE:
        area = None

    print(area)
    print()

    #Writing data to csv file for each country found
    csv_writer.writerow([country_name, capital, population, area])

#Closing csv file
csv_file.close()